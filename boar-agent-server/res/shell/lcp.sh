#!/bin/bash

#"功能: 拷贝文件"
#"$1, 原文件目录"
#"$2, 原文件名"
#"$3, 目标文件目录"
#"$4, 目标文件名"
#"$5, 压缩标识: null - 不压缩; 0 - .gz; 1 - .zip"


print_input_err()
{
	echo "功能: 拷贝文件"
	echo "\$1, 原文件目录"
	echo "\$2, 原文件名"
	echo "\$3, 目标文件目录"
	echo "\$4, 目标文件名"
	echo "\$5, 压缩标识: null - 不压缩; 0 - .gz; 1 - .zip"
	echo ""

	return 0
}

if [[ $# -lt 4 ]] || [[ $# -gt 5 ]]; then
	print_input_err
	exit 1
fi

is_suc()
{
	if [ $? -eq 0 ]; then
		echo "[INFO]: command $1 is success"
		return 0
	else
		echo ""
		echo "[ERROR]: command $1 is failed!!!"
		exit 1
	fi
}


src_path=$1
src_file=$2
src_path_file=${src_path}/${src_file}
dest_path=$3
dest_file=$4
deal_flag=$5

#for test
echo "start lcp.sh"
echo "input [$1], [$2], [$3], [$4], [$5]"
if [ ! -e "${src_path_file}" ]; then
	echo "file[$1] not found!"
	exit 1
fi


if [ ! -d "${dest_path}" ]; then
	mkdir -p ${dest_path}
	is_suc "mkdir -p [${dest_path}]"
fi

if [[ "${deal_flag}" == "" ]] || [[ "${deal_flag}" == null ]]; then
	cp -rf ${src_path_file} ${dest_path}/${dest_file}
	is_suc "cp -rf [${src_path_file}] [${dest_path}/${dest_file}]"
elif [[ "${deal_flag}" == "0" ]]; then
	dest_file=${dest_file}.tar.gz

	cd ${src_path}
	is_suc "cd [${src_path}]"

	tar -zcvf ${dest_file} ${src_file}
	is_suc "tar -zcvf [${dest_file}] [${src_file}]"

	mv ${dest_file} ${dest_path}
	is_suc "mv [${dest_file}] [${dest_path}]"
elif [[ "${deal_flag}" == "1" ]]; then
	dest_file=${dest_file}.zip

	cd ${src_path}
	is_suc "cd [${src_path}]"

	zip -q -r ${dest_file} ${src_file}
	is_suc "zip -q -r [${dest_file}] [${src_file}]"
	
	mv ${dest_file} ${dest_path}
	is_suc "mv [${dest_file}] [${dest_path}]"
else
	echo "input error!!!"
	print_input_err
	exit 1
fi

echo "end lcp.sh"
exit 0

