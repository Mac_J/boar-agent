#!/bin/bash
#$1: port

work_path=`dirname $0`
if [[ "$?" != "0" ]]; then
	echo "[ERROR]: CURLINE[$LINENO], dirname $0"
	exit 1
fi

. ${work_path}/env.conf
if [[ "$?" != "0" ]]; then
	echo "[ERROR]: CURLINE[$LINENO], . ${work_path}/env.conf"
	exit 1
fi

if [ $# -lt 1 ]; then
	start_port=${DEFAULT_PORT}
else
	start_port=$1
fi

cd ${work_path}/..
is_suc "cd ${work_path}/.."

LOG_DIR=${LOG_PATH}/${APP_NAME}/${start_port}
ret=`ls |grep ".jar" |grep "${APP_NAME}" |grep -v "grep" |tr '\n' ':'`
MAIN_JAR=`echo ${ret%:*}`
echo "[DEBUG]: CURLINE[$LINENO], MAIN_JAR[${MAIN_JAR}]"
RUN_COMMAND="java -classpath ${CLASSPATH}:${LIBPATH}:${MAIN_JAR} -Dlog.appender=FILE -Dlog.dir=${LOG_DIR} -Dport=${start_port} com.boarsoft.boar.agent.Main"
echo "RUN_COMMAND[${RUN_COMMAND}]"
 
pids=`ps -ef|grep "$RUN_COMMAND" | grep -v "grep"|awk '{print $2}'`
if [ "$pids" = "" ]; then
	echo "[INFO]: Application[$APP_NAME] is stopped !!"
else
	echo "[INFO]: Application[$APP_NAME] is running !!"
fi


exit 0

