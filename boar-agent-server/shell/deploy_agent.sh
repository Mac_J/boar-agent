#!/bin/bash

#功能: 部署agent;
#参数: app_name  - 应用名; <boar-deploy-agent>
#      zip_path  - 要部署的应用包所在目录; </home/app/deploy/temp>
#      app_path  - 应用部署目录; </home/app/deploy>
#      sbin_path - 应用目录下stop.sh和start.sh所在目录<sbin>
#例:   ./deploy_agent.sh "app_name=boar-deploy-agent&zip_path=/home/app/deploy/temp&app_path=/home/app/deploy&sbin_path=sbin"
#
#注意项: 
#

CMP_SEP_PAR="&"
CMP_SEP_VAL="="
NULL_VAL="null"
#-------------------------------------
#功能: 解析参数串, 并将key export
#$1 待解析参数串
#$2 各参数之间的分隔串
#$3 key与value间的分隔串
#注: 1. 各参数之间的分隔串允许有空格和换行
#    2. 各参数之间的分隔串之外不允许包含空格和换行
#    3. 各参数之间的分隔串与key与value间的分隔串不能相同
#    4. 若存在value未填值, 则默认填充'null'; <'xxx='替换为'xxx=null'>
get_para()
{
	local info=$1
	local sep_par=$2
	local sep_val=$3
	local sep_exp="="
	
	echo "get_para receive: [$1], [$2], [$3]"
	if [[ $# -lt 3 ]]; then
		echo "[ERROR]: CURLINE[$LINENO], input get_para() parameter error"
		return 1
	fi
	
	if [[ "${sep_val}" != "=" ]]; then
		info=`echo ${info} |sed "s/${sep_val}/${sep_exp}/g"`
	fi
	##added by OscarZhou 2017/04/13 PM
	info=`echo ${info} |sed "s/\"//g"`
	info=`echo ${info} |sed "s/${sep_par}/\n/g"`
	#echo "$info"
	for cur_par in ${info}
	do
		if [[ ${cur_par} == "" ]] || [[ ${cur_par} == "\n" ]]; then
			continue
		fi
		
		#key value中无分隔符时, 报错
		if [[ "${cur_par}" == "${cur_par%${sep_exp}*}" ]]; then
			echo "input err: cur_par[${cur_par}]"
		 	return 1
		fi
		
		#将'xxx='替换为'xxx=null'
		if [[ "" == "${cur_par#*${sep_exp}}" ]]; then
			cur_par="${cur_par}${NULL_VAL}"
		fi
		
		echo "cur: [$cur_par]"
		export ${cur_par}
	done
	
	return 0
}

is_suc()
{
    if [ $? -eq 0 ]; then
        echo "[INFO]: $1 is success"
        return 0
    else
        echo ""
        echo "[ERROR]: $1 is failed!!!"
        exit 1
    fi
}


#$1: zip path
#$2: app path
#$3: sbin path
#$4: zip name
#$5: app name
first_deploy()
{
	local z_path=$1
	local a_path=$2
	local s_path=$3
	local zipname=$4
	local appname=$5
	
	cd ${a_path}
	is_suc "CURLINE[$LINENO], cd ${a_path}"
	
	mv ${z_path}/${zipname} ${a_path}
	is_suc "CURLINE[$LINENO], mv ${z_path}/${zipname} ${a_path}"
	
	unzip ${zipname}
	is_suc "CURLINE[$LINENO], unzip ${zipname}"
	
	cd ${appname}*/${s_path}
	is_suc "CURLINE[$LINENO], cd ${appname}*/${s_path}"
	
	sh start.sh
	is_suc "CURLINE[$LINENO], start.sh"
	
	rm ${a_path}/${zipname}
	is_suc "CURLINE[$LINENO], rm ${a_path}/${zipname}"
	
	return 0
}


#$1: zip path
#$2: app path
#$3: sbin path
#$4: zip name
#$5: app name
#$6: dir name
deploy()
{
	local z_path=$1
	local a_path=$2
	local s_path=$3
	local zipname=$4
	local appname=$5
	local dirname=$6
	
	cd ${a_path}/${dirname}/${s_path}
	is_suc "CURLINE[$LINENO], cd ${a_path}/${dirname}/${s_path}"
	
	sh stop.sh
	is_suc "CURLINE[$LINENO], sh stop.sh"
	
	cd ${a_path}
	is_suc "CURLINE[$LINENO], cd ${a_path}"
	
	rm -rf ${dirname}
	is_suc "CURLINE[$LINENO], rm -rf ${dirname}"
	
	mv ${z_path}/${zipname} ${a_path}
	is_suc "CURLINE[$LINENO], mv ${z_path}/${zipname} ${a_path}"
	
	unzip ${zipname}
	is_suc "CURLINE[$LINENO], unzip ${zipname}"
	
	cd ${appname}*/${s_path}
	is_suc "CURLINE[$LINENO], cd ${appname}*/${s_path}"
	
	sh start.sh
	is_suc "CURLINE[$LINENO], start.sh"
	
	rm ${a_path}/${zipname}
	is_suc "CURLINE[$LINENO], rm ${a_path}/${zipname}"
	
	return 0
}


echo "##########start deploy agent##########"

#加载配置文件
env_path=`dirname $0`
if [[ -f ${env_path}/env.conf ]]; then
	echo "[DEBUG]: CURLINE[$LINENO], . ${env_path}/env.conf"
	. ${env_path}/env.conf
fi

get_para "$*" ${CMP_SEP_PAR} ${CMP_SEP_VAL}
#echo "[INFO]: app_name[$app_name], zip_path[$zip_path], app_path[app_path]"

if [[ "$app_name" == "null" ]] || [[ "$app_name" == "" ]]; then
	if [[ "$APP_NAME" == "null" ]] || [[ "$APP_NAME" == "" ]]; then
		echo "[ERROR]: CURLINE[$LINENO], app_name"
		exit 1
	fi
	app_name=${APP_NAME}
	echo "[DEBUG]: CURLINE[$LINENO], app_name not input, [${app_name}]"
fi

if [[ "$zip_path" == "null" ]] || [[ "$zip_path" == "" ]]; then
	if [[ "$TEMP_HOME" == "null" ]] || [[ "$TEMP_HOME" == "" ]]; then
		echo "[ERROR]: CURLINE[$LINENO], zip_path"
		exit 1
	fi
	zip_path=${TEMP_HOME}
	echo "[DEBUG]: CURLINE[$LINENO], zip_path not input, [${zip_path}]"
fi

if [[ "$app_path" == "null" ]] || [[ "$app_path" == "" ]]; then
	if [[ "$APP_HOME" == "null" ]] || [[ "$APP_HOME" == "" ]]; then
		echo "[ERROR]: CURLINE[$LINENO], app_path"
		exit 1
	fi
	app_path=${APP_HOME}
	echo "[DEBUG]: CURLINE[$LINENO], app_path not input, [$app_path]"
fi

if [[ "$sbin_path" == "null" ]] || [[ "$sbin_path" == "" ]]; then
	if [[ "$SBIN_DIR" == "null" ]] || [[ "$SBIN_DIR" == "" ]]; then
		echo "[ERROR]: CURLINE[$LINENO], sbin_path"
		exit 1
	fi
	sbin_path=${SBIN_DIR}
	echo "[DEBUG]: CURLINE[$LINENO], sbin_path not input, [$sbin_path]"
fi

if [ ! -d ${zip_path} ]; then
	echo "[ERROR]: CURLINE[$LINENO], zip_path[${zip_path}] not found"
	exit 1
fi

if [ ! -d ${app_path} ]; then
	mkdir -p ${app_path}
	is_suc "CURLINE[$LINENO], mkdir -p ${app_path}"
fi

ret=`ls ${zip_path} |grep "${app_name}" |grep ".zip" |grep -v "grep" |wc -l`
is_suc "CURLINE[$LINENO], get zip num"
if [[ "$ret" != "1" ]]; then
	echo "[ERROR]: CURLINE[$LINENO], zip num[$ret]"
	exit 1
fi
zip_name=`ls ${zip_path} |grep "${app_name}" |grep ".zip" |grep -v "grep"`
is_suc "CURLINE[$LINENO], get zip name"

ret=`ls -F ${app_path} |grep "/$" |grep ${app_name} |wc -l`
is_suc "CURLINE[$LINENO], get dirname"
dir_name=`ls -F ${app_path} |grep "/$" |grep ${app_name} |awk '{print}'`
is_suc "CURLINE[$LINENO], get dir name"
if [[ "${ret}" == "1" ]]; then
	echo "[DEBUG]: CURLINE[$LINENO]"
	deploy $zip_path $app_path $sbin_path $zip_name $app_name $dir_name
	is_suc "CURLINE[$LINENO], deploy()"
elif [[ "${ret}" == "0" ]]; then
	echo "[DEBUG]: CURLINE[$LINENO]"
	first_deploy $zip_path $app_path $sbin_path $zip_name $app_name
	is_suc "CURLINE[$LINENO], first_deploy()"
else
	echo "[ERROR]: CURLINE[$LINENO], get ${app_path}/${app_name} num[$ret]"
	ls ${app_path} |grep ${app_name} 
	exit 1
fi


echo "##########deploy agent SUC##########"
exit 0

