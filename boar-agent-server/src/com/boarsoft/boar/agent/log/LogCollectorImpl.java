package com.boarsoft.boar.agent.log;

import java.util.Set;

import javax.annotation.PostConstruct;

public class LogCollectorImpl implements LogCollector {
	/** */
	protected Set<LogDirWorker> workerSet;

	@PostConstruct
	public void init() {
		for (LogDirWorker w : workerSet) {
			Thread t = new Thread(w);
			t.setDaemon(true);
			t.start();
		}
	}

	public void addWorker(LogDirWorker worker) {
		workerSet.add(worker);
	}

	public Set<LogDirWorker> getWorkerSet() {
		return workerSet;
	}

	public void setWorkerSet(Set<LogDirWorker> workerSet) {
		this.workerSet = workerSet;
	}
}
