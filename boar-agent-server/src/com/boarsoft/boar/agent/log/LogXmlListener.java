package com.boarsoft.boar.agent.log;

import com.boarsoft.config.core.ConfigFileListener;
import com.boarsoft.config.spring.SpringXmlListener;

public class LogXmlListener extends SpringXmlListener implements ConfigFileListener {

	@Override
	public boolean onReady(String rp) {
		if (!super.onReady(rp)) {
			return false;
		}
		// 检查需要的bean是否准备好了，执行必要的动作
		appCtx.getBean(LogCollector.class);
		return true;
	}
}