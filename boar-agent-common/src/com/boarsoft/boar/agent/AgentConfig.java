package com.boarsoft.boar.agent;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AgentConfig {
	private final static Logger log = LoggerFactory.getLogger(AgentConfig.class);
	private static Properties prop;

	/** */
	public static String FILE_PATH = "files";
	/** */
	public static String SHELL_PATH = "shells";

	static {
		if (prop == null) {
			InputStream is = AgentConfig.class.getClassLoader().getResourceAsStream("conf/config.properties");
			if (is == null) {
				log.warn("Can not init AgentConfig/config.properties");
			} else {
				log.info("Init AgentConfig with default config.properties");
				AgentConfig.init(is);
			}
		}
	}

	public static void init(InputStream is) {
		if (prop == null) {
			prop = new Properties();
			try {
				prop.load(is);
				FILE_PATH = prop.getProperty("agent.file.path", FILE_PATH);
				SHELL_PATH = prop.getProperty("agent.shell.path", SHELL_PATH);
			} catch (IOException e) {
				log.error("Error on load system config.", e);
				throw new RuntimeException(e);
			}
		}
	}

	public static String getProperty(String name) {
		return prop.getProperty(name);
	}

}