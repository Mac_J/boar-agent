package com.boarsoft.boar.agent.plugin;

import com.boarsoft.flow.core.bean.ProcessHandler;

public interface TableHandler extends ProcessHandler {

	/**
	 * TODO 将文件中的数据读取，插入或更新表（可指定的字段）
	 * 
	 * @param data
	 */
	void restore(Object data);

	/**
	 * TODO 执行SQL，将查询结果保存到文件（可指定的字段）
	 * 
	 * @param data
	 */
	void backup(Object data);
}
