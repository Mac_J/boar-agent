package com.boarsoft.boar.agent.log;

import java.io.File;
import java.util.Date;

/**
 * 日志采集器，日志文件信息类
 * 
 * @author Mac_J
 *
 */
public class LogFileMeta {
	protected File file;
	protected long length;
	protected long lastTime;
	protected long creationTime;
	protected long offset;
	protected StringBuilder buffer = new StringBuilder();

	@Override
	public String toString() {
		return new StringBuilder().append(creationTime)//
				.append("/").append(file).toString();
	}

	public void update() {
		lastTime = new Date().getTime();
		length = (file == null ? 0 : file.length());
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public long getLastTime() {
		return lastTime;
	}

	public void setLastTime(long last) {
		this.lastTime = last;
	}

	public long getOffset() {
		return offset;
	}

	public void setOffset(long offset) {
		this.offset = offset;
	}

	public StringBuilder getBuffer() {
		return buffer;
	}

	public void setBuffer(StringBuilder buffer) {
		this.buffer = buffer;
	}

	public long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(long creationTime) {
		this.creationTime = creationTime;
	}

	public long getLength() {
		return length;
	}

	public void setLength(long length) {
		this.length = length;
	}
}
