package com.boarsoft.boar.agent.log;

import java.io.IOException;

public interface LogDirWorker extends Runnable {
	/**
	 * 
	 * @return
	 */
	long getInterval();

	/**
	 * 
	 * @param dirMeta
	 * @return
	 * @throws IOException
	 */
	int watch(LogDirMeta dirMeta) throws IOException;
}
