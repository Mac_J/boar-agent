package com.boarsoft.boar.agent;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProcessStreamReader implements Runnable {
	private Logger log = LoggerFactory.getLogger(ProcessStreamReader.class);
	protected BufferedReader br;
	protected StringBuilder sb = new StringBuilder();

	public ProcessStreamReader(InputStream is, String charset) throws UnsupportedEncodingException {
		br = new BufferedReader(new InputStreamReader(is, charset));
	}

	@Override
	public void run() {
		String ln = null;
		try {
			while ((ln = br.readLine()) != null) {
				sb.append(ln).append("\n");
				log.info(ln);
			}
			sb.setLength(Math.max(0, sb.length() - 1));
		} catch (Exception e) {
			log.error("Error on read stream from process", e);
		}
	}

	public String getResult() {
		return sb.toString();
	}
}
