package com.boarsoft.agent;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import com.boarsoft.bean.ReplyInfo;

public interface AgentService {
	/**
	 * @param dbId
	 *            数据库ID，对应 conf/config.properties 中的配置项
	 * @param path
	 *            sql文件的相对路径
	 * @param taskKey
	 *            用户某次操作的唯一标识，用于将一次操作的全部日志归档，在切面中记录。
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	ReplyInfo<Object> executeSql(String dbId, String path, String taskKey) throws UnsupportedEncodingException, IOException;

	/**
	 * 启动一个流程
	 * 
	 * @param entry
	 * @param data
	 * @return
	 * @throws Throwable
	 */
	ReplyInfo<Object> start(String entry, Object data) throws Throwable;

	/**
	 * 执行预置的SHELL
	 * 
	 * @param code
	 *            预置的SHELL的名字，如：lcp
	 * @param args
	 * @param taskKey
	 *            用户某次操作的唯一标识，用于将一次操作的全部日志归档，在切面中记录。
	 * @return
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	ReplyInfo<Object> executeCmd(String code, String[] args, String taskKey) throws IOException, InterruptedException;

	/**
	 * 执行 parent 目录下的 本地SHELL脚本
	 * 
	 * @param parent
	 *            父级的路径
	 * @param path
	 *            相对于父级的路径
	 * @param args
	 * @param taskKey
	 *            用户某次操作的唯一标识，用于将一次操作的全部日志归档，在切面中记录。
	 * @return
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	ReplyInfo<Object> executeShell(String parent, String path, String[] args, String taskKey) throws IOException, InterruptedException;

	/**
	 * 调用agentC远程执行脚本
	 * 
	 * @param taskKey
	 *            用户某次操作的唯一标识，用于将一次操作的全部日志归档，在切面中记录。
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	ReplyInfo<Object> executeShell(String user, String addr, String parent, String path, String[] args, String taskKey) throws IOException, InterruptedException;

	/**
	 * 执行 AgentConfig.FILE_PATH下 的 SHELL脚本
	 * 
	 * @param path
	 *            相对于 AgentConfig.FILE_PATH 的 path
	 * @param args
	 * @param taskKey
	 *            用户某次操作的唯一标识，用于将一次操作的全部日志归档，在切面中记录。
	 * @return
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	ReplyInfo<Object> executeShell(String path, String[] args, String taskKey) throws IOException, InterruptedException;

	/**
	 * 执行任意脚本或命令。如：sh /home/app/check.sh arg0 arg1
	 * 
	 * @param cmds
	 *            如：[ "sh", "/home/app/check.sh", "arg0", "arg1" ]
	 * @return
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	ReplyInfo<Object> executeShell(String[] cmds) throws IOException, InterruptedException;

	/**
	 * 执行任意脚本或命令。如：“sh /home/app/check.sh arg0 arg1”
	 * 
	 * @return
	 * @throws InterruptedException
	 * @throws IOException
	 */
	ReplyInfo<Object> executeShell(String cmd) throws IOException, InterruptedException;

	/**
	 * 用于远程执行某个语句
	 * 
	 * @param user
	 *            执行SHELL所用用户名
	 * @param addr
	 *            目标服务器地址
	 * @param cmd
	 *            要执行的SHELL命令
	 * @param taskKey
	 *            用户某次操作的唯一标识，用于将一次操作的全部日志归档，在切面中记录。
	 * @return
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	ReplyInfo<Object> executeShell2(String user, String addr, String cmd, String taskKey) throws IOException, InterruptedException;

	// ---------------------------

	/**
	 * 执行 AgentConfig.SHELL_PATH 下 path 子目录的 SHELL脚本
	 * 
	 * @param path
	 *            相对于 AgentConfig.SHELL_PATH 的 path
	 * @param args
	 * @return
	 * @throws InterruptedException
	 * @throws IOException
	 */
	ReplyInfo<Object> executeCmd(String path, String[] args) throws IOException, InterruptedException;

	/**
	 * 执行 AgentConfig.FILE_PATH 下 path 子目录的 SHELL脚本
	 * 
	 * @param path
	 *            相对于 AgentConfig.FILE_PATH 的 path
	 * @param args
	 * @return
	 */
	ReplyInfo<Object> executeShell(String path, String[] args) throws Throwable;

	/**
	 * 用于远程拷贝文件
	 * 
	 * @param index
	 *            为0表示从头写入文件，否则追加
	 * @param to
	 *            目标文件名
	 * @param buf
	 *            数据
	 * @param off
	 *            数据中的起始偏移量
	 * @param len
	 *            要写入的字节数
	 */
	void write2(int index, String to, byte[] buf, int off, int len) throws Throwable;

	/**
	 * 检查文件或目录是否存在
	 * 
	 * @param pp
	 * @return
	 */
	boolean exists(String path);

	/**
	 * 用于调用AgentC，从打包服务器复制安装包到目标节点
	 * 
	 * @param from
	 *            原文件在打包服务器上的路径
	 * @param to
	 *            目标节点上的路径
	 * @param ip
	 *            目标服务器的IP
	 * @param port
	 *            目标服务器AgentX的端口，因为AgentC并没有AgentX的端口信息
	 * @return
	 */
	void copy2(String from, String to, String ip, int port) throws Throwable;

	/**
	 * 用于调用AgentC，从打包服务器复制安装包到多个目标节点
	 * 
	 * @param from
	 *            原文件在打包服务器上的路径
	 * @param to
	 *            目标节点上的路径
	 * @param ipLt
	 *            目标服务器的IP列表
	 * @param port
	 *            目标服务器AgentX的端口，因为AgentC并没有AgentX的端口信息
	 * @return
	 */
	ReplyInfo<Object> copy2(String from, String to, List<String> ipLt, int port) throws Throwable;

	/**
	 * 
	 * @param zip
	 * @param to
	 * @throws IOException
	 */
	ReplyInfo<Object> unzip(String zip, String to) throws IOException;

	/**
	 * 
	 * @param path
	 * @return
	 * @throws IOException
	 */
	ReplyInfo<Object> delete(String path) throws IOException;

	/**
	 * 
	 * @param parent
	 * @param path
	 * @param args
	 * @return
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	ReplyInfo<Object> executeShell(String parent, String path, String[] args) throws IOException, InterruptedException;

	/**
	 * 远程拷贝
	 * 
	 * @param from
	 * @param to
	 * @param addr
	 * @throws Throwable
	 */
	void copy2(String from, String to, String addr) throws Throwable;

	/**
	 * 执行本地拷贝
	 * 
	 * @param from
	 * @param to
	 * @return
	 * @throws Throwable
	 */
	ReplyInfo<Object> copy2(String from, String to) throws Throwable;
}