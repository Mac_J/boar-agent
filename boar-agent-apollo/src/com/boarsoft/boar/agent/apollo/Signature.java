package com.boarsoft.boar.agent.apollo;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * 修改自com.ctrip.framework.apollo.core.signature.Signature
 * 
 * @author Mac_J
 */
public class Signature {

	/**
	 * Authorization=Apollo {appId}:{sign}
	 */
	private static final String AUTHORIZATION_FORMAT = "Apollo %s:%s";
	private static final String DELIMITER = "\n";

	public static final String HTTP_HEADER_AUTHORIZATION = "Authorization";
	public static final String HTTP_HEADER_TIMESTAMP = "Timestamp";

	public static String signature(String timestamp, String pathWithQuery, String secret) {
		String stringToSign = timestamp + DELIMITER + pathWithQuery;
		return HmacSha1Utils.signString(stringToSign, secret);
	}

	public static Map<String, String> buildHttpHeaders(String url, String appId, String secret) {
		long currentTimeMillis = System.currentTimeMillis();
		String timestamp = String.valueOf(currentTimeMillis);
		String pathWithQuery = url2PathWithQuery(url);
		// appId: 应用的appId
		// signature：使用访问密钥对当前时间以及所访问的URL加签后的值，具体实现可以参考Signature.signature
		String signature = signature(timestamp, pathWithQuery, secret);
		Map<String, String> headers = new HashMap<>();
		headers.put(HTTP_HEADER_AUTHORIZATION, String.format(AUTHORIZATION_FORMAT, appId, signature));
		headers.put(HTTP_HEADER_TIMESTAMP, timestamp);
		return headers;
	}

	private static String url2PathWithQuery(String urlString) {
		try {
			URL url = new URL(urlString);
			String path = url.getPath();
			String query = url.getQuery();

			String pathWithQuery = path;
			if (query != null && query.length() > 0) {
				pathWithQuery += "?" + query;
			}
			return pathWithQuery;
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("Invalid url pattern: " + urlString, e);
		}
	}
}