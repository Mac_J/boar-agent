package com.boarsoft.boar.agent.apollo;

import java.util.Map;

public class NamespaceMeta {
	protected String appId;
	protected String cluster;
	protected String namespaceName;
	protected Map<String, String> configurations;
	protected String releaseKey;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getCluster() {
		return cluster;
	}

	public void setCluster(String cluster) {
		this.cluster = cluster;
	}

	public String getNamespaceName() {
		return namespaceName;
	}

	public void setNamespaceName(String namespaceName) {
		this.namespaceName = namespaceName;
	}

	public Map<String, String> getConfigurations() {
		return configurations;
	}

	public void setConfigurations(Map<String, String> configurations) {
		this.configurations = configurations;
	}

	public String getReleaseKey() {
		return releaseKey;
	}

	public void setReleaseKey(String releaseKey) {
		this.releaseKey = releaseKey;
	}
}